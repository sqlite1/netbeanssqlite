/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nutthawut.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OMEN
 */
public class InsertUser {
public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user1.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (1, 'Nutthawut', '123456');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (2, 'Siriyakorn', '0970126919');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (3, 'Taneat', '02542021');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (ID,USERNAME,PASSWORD) "
                    + "VALUES (4, 'Keytawat', '987654321');";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
             System.out.println("No library org.sqlite.JDBC!!!");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
        }
    }
}
